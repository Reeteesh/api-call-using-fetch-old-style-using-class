import React, { Component } from "react";
import axios from "axios";

class App extends Component {
  state = {
    heading: "Fetch Example API using Fetch",
    isloading: true,
    isError: false,
    items: [],
    character: {},
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {

    axios.get("https://jsonplaceholder.typicode.com/users")
      .then((response) => {
        // handle success

        console.log("success", response.data);
       
        this.setState({
          items: response.data,
          isloading: false,
        });
      })
      .catch((error) => {
        // handle error
        console.log("error", error);
      });
  };

  render() {
    var { isloading, items, isError } = this.state;
    if (isloading) {
      return <div>loading....</div>;
    } else if (isError) {
      return <div>Error...</div>;
    } else {
      return (
        <div>
          <ul>
            {items.map((item) => {
              return (
                <li key={item.id}>
                  Name: {item.name} Email:{item.email}
                </li>
              )
            }
            )}
          </ul>
        </div>
      );
    }
  }
}

export default App;
